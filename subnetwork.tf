resource "google_compute_subnetwork" "subnetwork" {
  count                    = "${var.subnetwork_name == "" ? 1 : 0}"
  enable_flow_logs         = "${var.enable_flow_logs}"
  name                     = "${format("%v-%v", var.name, var.environment)}"
  network                  = "${var.vpc}"
  project                  = "${var.project}"
  region                   = "${var.region}"
  ip_cidr_range            = "${var.ip_cidr_range}"
  private_ip_google_access = true
}
